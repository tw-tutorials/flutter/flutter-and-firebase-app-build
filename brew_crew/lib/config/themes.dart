import 'package:flutter/material.dart';

class Themes {
  static final ThemeData lightTheme = ThemeData(
    primarySwatch: Colors.brown,
    scaffoldBackgroundColor: Colors.brown[100],
    appBarTheme: AppBarTheme(
      elevation: 0,
    ),
    primaryColor: Colors.brown,
    primaryTextTheme: TextTheme(
      button: TextStyle(
        color: Colors.white,
      ),
    ),
  );

  static final ThemeData darkTheme = ThemeData.dark();
}
